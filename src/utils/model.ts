export const MusicPlaylist = [
  {
    title: "01. Service Bell",
    artist: "Daniel Simion",
    url:
      "https://raw.githubusercontent.com/muhammederdem/mini-player/master/mp3/1.mp3",
    image: "https://source.unsplash.com/crs2vlkSe98/400x400",
  },
  {
    title: "02. Hyena Laughing",
    artist: "Daniel Simion",
    url: "https://soundbible.com/mp3/hyena-laugh_daniel-simion.mp3",
    image: "https://source.unsplash.com/Esax9RaEl2I/400x400",
  },
  {
    title: "03. Hyena Laughing",
    artist: "Daniel Simion",
    url: "https://soundbible.com/mp3/hyena-laugh_daniel-simion.mp3",
    image: "https://source.unsplash.com/Esax9RaEl2I/400x400",
  },
];
